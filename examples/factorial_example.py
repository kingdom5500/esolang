import esolang


PROGRAM [

    INCLUDE: "core",

    PUSH: FUNCTION [
        SHOULD (ARGS != 1) [
            PUSH: "factorial only takes one argument",
            ERROR,
        ],

        POP: number,

        # return 1 if we've reached the end
        SHOULD (number <= 1) [
            PUSH: 1,
            FINISH,
        ],

        # else, do the factorial recursion
        OTHERWISE [
            PUSH: factorial,
            PUSH: number - 1,
            CALL: 1,

            PUSH: number * POP,
            FINISH,
        ]
    ],

    POP: factorial,

    # main part of the program...
    PUSH: FUNCTION[
        PUSH: core.print,
        PUSH: "Result:",
        PUSH: factorial,
        PUSH: core.int,
        PUSH: core.input,
        PUSH: "Enter a number: ",
        CALL: 1,
        CALL: 1,
        CALL: 1,
        CALL: 2,
    ],

    CALL: 0,
]
