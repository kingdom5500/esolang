import esolang


PROGRAM [
    INCLUDE: "fileio",
    INCLUDE: "core",

    # open the file for writing.
    PUSH: fileio.open,
    PUSH: "my_file.txt",
    PUSH: fileio.WRITE,
    CALL: 2,

    POP: file,

    # write something to it.
    PUSH: fileio.write,
    PUSH: file,
    PUSH: "hello file!",
    CALL: 2,

    # close the file.
    PUSH: fileio.close,
    PUSH: file,
    CALL: 1,

    # open the file for reading.
    PUSH: fileio.open,
    PUSH: "my_file.txt",
    PUSH: fileio.READ,
    CALL: 2,

    POP: file,

    # read 5 bytes of the file.
    PUSH: fileio.read,
    PUSH: file,
    PUSH: 5,
    CALL: 2,

    POP: text,

    # close the file.
    PUSH: fileio.close,
    PUSH: file,
    CALL: 1,

    # print the text we read.
    PUSH: core.print,
    PUSH: text,
    CALL: 1,
]
