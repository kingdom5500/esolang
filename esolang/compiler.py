import os.path

from esolang.tokens import tokenise
from esolang.parse import TokenParser
from esolang.utils import EsoError


def parse_module(module: str):
    """
    Parse a module. The module must be in the "modules"
    folder relative to this file.

    :param module: The module's name (like "core").
    :return: The module's full AST, if it's valid.
    """

    eso_dir = os.path.dirname(__file__)
    file_name = f"{module}.py"

    module_file = os.path.join(eso_dir, "modules", file_name)

    with open(module_file) as file:
        source = file.read()

    tokens = list(tokenise(source))

    if tokens[0].typ != "MODULE":
        raise EsoError("attempted to include a non-module")

    return TokenParser(tokens).parse()


# the transformer uses the above function, so this
# is here to avoid the mess of circular imports.
from esolang.transformer import Transformer  # noqa


def compile_source(source: str):
    """
    Compile some source code to actual python code.

    :param module: The source code to be compiled.
    :return: The compiled python code.
    """

    tokens = tokenise(source)
    astree = TokenParser(tokens).parse()
    return Transformer().transform(astree)
