import __main__

from esolang.compiler import compile_source

if __name__ == "__main__":
    print("This script must be imported! :(")
    exit()

# read the source of the file that imported this script
with open(__main__.__file__) as file:
    source = file.read().replace("import esolang", "").strip()


py_source = compile_source(source)
exec(py_source, vars(__main__))

# stop the rest of the script.
exit(0)
