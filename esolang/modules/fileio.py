MODULE: fileio [

    PUSH: FUNCTION [
        SHOULD (ARGS != 1) [
            PUSH: "fileio.touch required 1 argument (file_name)",
            ERROR,
        ],

        POP: file_name,
        __PYTHON__: "open(file_name, 'a').close()",
    ],

    POP: touch_file,

    # file access flags from the OS module
    PUSH: 1, POP: READ,
    PUSH: 2, POP: WRITE,

    # open file as read and/or write
    PUSH: FUNCTION [
        SHOULD (ARGS != 2) [
            PUSH: "fileio.open requires 2 arguments (file_name, access_type)",
            ERROR,
        ],

        # grab the access_type argument
        POP: access_type,
        POP: file_name,

        # i should create some form of unary operators
        SHOULD ((access_type < fileio.READ) | (access_type > fileio.WRITE)) [
            PUSH: "fileio.open was given an invalid access_type",
            ERROR,
        ],

        PUSH: fileio.touch_file,
        PUSH: file_name,
        CALL: 1,

        PUSH: access_type & fileio.READ,
        PUSH: access_type & fileio.WRITE,
        POP: file_write,
        POP: file_read,

        # i should also make something like if/elif/else... should be easy.
        SHOULD (file_write & file_read) [
            PUSH: __PYTHON__: "open(file_name, 'r+')",
        ],

        OTHERWISE [
            SHOULD (file_write) [
                PUSH: __PYTHON__: "open(file_name, 'w')"
            ],
            OTHERWISE [
                PUSH: __PYTHON__: "open(file_name)"
            ],
        ]
    ],

    POP: open,

    PUSH: FUNCTION [
        SHOULD (ARGS != 1) [
            PUSH: "fileio.close requires 1 argument (file)",
            ERROR,
        ],

        __PYTHON__: "_eso_stack.pop().close()"
    ],

    POP: close,

    PUSH: FUNCTION [
        SHOULD (ARGS != 2) [
            PUSH: "fileio.write requires 2 arguments (file, text)",
            ERROR,
        ],

        POP: text,
        POP: file,

        __PYTHON__: "file.write(text)",
    ],

    POP: write,

    PUSH: FUNCTION [
        SHOULD (ARGS != 2) [
            PUSH: "fileio.write requires 2 arguments (file, amount)",
            ERROR,
        ],

        POP: amount,
        POP: file,

        PUSH: __PYTHON__: "file.read(amount)",
    ],

    POP: read
]
