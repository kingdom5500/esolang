MODULE: core [
    # function for clearing the stack.
    PUSH: FUNCTION [
        # if we haven't been given an argument,
        # clear the stack entirely.
        SHOULD (ARGS == 0) [
            POP: null,

            # recurse if there's more to pop.
            SHOULD (DEPTH > 0) [
                PUSH: core.empty_stack,
                CALL: 0,
            ],
        ],

        # if we've been given an argument, clear
        # exactly that many items from the stack.
        OTHERWISE [
            POP: remaining,
            POP: null,

            # repeat if we have more items to pop.
            SHOULD ((DEPTH > 0) * (remaining > 0)) [
                PUSH: core.empty_stack,
                PUSH: remaining - 1,
                CALL: 1,
            ]
        ],
    ],

    POP: empty_stack,

    # function for printing to the screen.
    PUSH: FUNCTION [
        __PYTHON__: "print(*_eso_stack[-_ARGS:])",

        # consume the arguments.
        PUSH: core.empty_stack,
        PUSH: ARGS - 1,
        CALL: 1,
    ],

    POP: print,

    # function for getting user input.
    PUSH: FUNCTION [
        SHOULD (ARGS == 1) [
            PUSH: __PYTHON__: "input(_eso_stack.pop())"
        ],

        # otherwise, just do a plain input.
        OTHERWISE [
            PUSH: __PYTHON__: "input()",
        ],
    ],

    POP: input,

    # next 3 functions are for typecasting.
    PUSH: FUNCTION [
        SHOULD (ARGS != 1) [
            PUSH: "int requires one argument",
            ERROR,
        ],

        PUSH: __PYTHON__: "int(_eso_stack.pop())"
    ],

    POP: int,

    PUSH: FUNCTION [
        SHOULD (ARGS != 1) [
            PUSH: "float requires one argument",
            ERROR,
        ],

        PUSH: __PYTHON__: "float(_eso_stack.pop())"
    ],

    POP: float,

    PUSH: FUNCTION [
        SHOULD (ARGS != 1) [
            PUSH: "str requires one argument",
            ERROR,
        ],

        PUSH: __PYTHON__: "str(_eso_stack.pop())"
    ],

    POP: str,
]
