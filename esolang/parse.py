from typing import List

from esolang.tokens import Token
from esolang.utils import EsoError, KEYWORDS, SIMPLE_TOKENS


class TokenParser:
    def __init__(self, tokens: List[Token]):
        self.tokens = list(tokens)

    @staticmethod
    def matches(actual: str, *expected: str) -> bool:
        """
        A helper method to check if a token is an expected type.

        :param actual: The token's actual type.
        :param expected: The token types to be expected.
        :return: True if the type is correct, else False.
        """

        is_any_kw = False

        # if we just expect any keyword, instead of a specific one.
        if "KEYWORD" in expected:
            is_any_kw = actual in KEYWORDS

        return is_any_kw or actual in expected

    def panic(self, message: str):
        """
        Raise an EsoError with a given message. This automatically
        adds the (line, column) of the current token. There is no
        return value because an exception is always raised.

        :param message: The message of the exception.
        """

        try:
            token = self.tokens[0]
        except IndexError:
            line, col = "N/A", "N/A"
        else:
            line, col = token.line, token.column

        raise EsoError(message, line, col)

    def peek(self, *expected: str, offset: int = 0) -> bool:
        """
        Check if the token at a given offset is an expected type.

        :param expected: The token types that are expected.
        :param offset: The index of the list to be peeked.
        :return: True if the token was a valid type, else False.
            Will return False if the offset doesn't exist in the list.
        """

        try:
            typ = self.tokens[offset].typ
        except IndexError:
            return False

        return self.matches(typ, *expected)

    def consume(self, *expected: str) -> Token:
        """
        Pop a token from the list if it's an expected type.
        This will cause a panic if the token type is not valid.

        :param expected: The token types that are expected.
        :return: The next Token object in the list.
        """

        try:
            token = self.tokens.pop(0)
        except IndexError:
            self.panic("expected more tokens")

        if self.matches(token.typ, *expected):
            return token

        self.panic(f"unexpected token {token.value!r}")

    def parse(self):
        """
        Parse a full list of tokens. The first token must be
        a PROGRAM or MODULE token, otherwise it will panic.

        :return: The full AST
        """

        if not self.peek("PROGRAM", "MODULE"):
            self.panic("expected a PROGRAM or MODULE token")

        # PROGRAM is a keyword, so it's just handled there.
        return self.parse_keyword()

    def parse_block(self):
        """
        Parse a block. The first token must be BLOCK_OPEN,
        otherwise it will panic. This will parse until it
        meets a BLOCK_CLOSE, and will consume a leading COMMA
        if there is one.

        :return: A list of keyword nodes.
        """

        nodes = []

        # make sure theres an opening [ here.
        if not self.peek("BLOCK_OPEN"):
            self.panic("expected a block")

        self.consume("BLOCK_OPEN")

        # blocks are just a list of expressions.
        while not self.peek("BLOCK_CLOSE"):
            nodes.append(self.parse_keyword())

        self.consume("BLOCK_CLOSE")

        # blocks end with a comma, except for PROGRAM blocks.
        if self.peek("COMMA"):
            self.consume("COMMA")

        return nodes

    def parse_simple(self):
        """
        Parse a simple token. Find the utils.SIMPLE_TOKENS
        tuple to see what is considered to be a simple token.

        :return: A simple node, containing a type and a value.
        """

        if not self.peek(*SIMPLE_TOKENS):
            self.panic("expected a simple token")

        token = self.consume(*SIMPLE_TOKENS)

        node = {
            "type": SIMPLE_TOKENS[token.typ],
            "value": token.value
        }

        return node

    def parse_expr(self, *, has_parens: bool = False):
        """
        Parse an expression.

        :param has_parens: Set to True for checking parameters.
        :return: An expression node.
        """

        items = []

        if has_parens:
            # if we expect parentheses but don't get any, panic.
            if not self.peek("EXPR_OPEN"):
                self.panic("expected expression parentheses")

            self.consume("EXPR_OPEN")

        while True:
            # if there's expression parentheses, parse 'em.
            if self.peek("EXPR_OPEN"):
                items.append(self.parse_expr(has_parens=True))

            # parse any possible operands
            elif self.peek("NUMBER", "STRING", "NAME"):
                items.append(self.parse_simple())

            # these keywords can be used as operands. parse them.
            elif self.peek("POP", "ARGS", "DEPTH"):
                kw_node = self.parse_keyword()

                # POP nodes can have a variable, which we don't want here.
                if kw_node.get("var") is not None:
                    self.panic("can't POP to var inside an expression")

                items.append(kw_node)

            # if we didn't get any operands, well fuck!
            else:
                self.panic("invalid operand")

            # we want to look for closing parentheses if specified.
            if has_parens and self.peek("EXPR_CLOSE"):
                self.consume("EXPR_CLOSE")
                break

            # check for the end of an expression
            elif self.peek("COMMA", "BLOCK_CLOSE"):
                if has_parens:
                    # we expected an EXPR_CLOSE, so panic.
                    self.panic("unexpected end of expression")

                break

            # operators are good, we'll allow them here.
            elif self.peek("OP"):
                items.append(self.parse_simple())

            # if we got something else, literally die.
            else:
                self.panic("expected expression delimiter or operator")

        if self.peek("COMMA"):
            self.consume("COMMA")

        node = {
            "type": "expression",
            "body": items,
            "parens": has_parens
        }

        return node

    def parse_keyword(self):
        """
        Parse a keyword. This method is a mess, but I don't care.

        :return: A keyword node.
        """

        def assert_terminator():
            """
            A helper method to check if we have a keyword terminator.
            """

            # check for terminator, remove COMMA if present.
            if self.peek("BLOCK_CLOSE", "COMMA"):
                if self.peek("COMMA"):
                    self.consume("COMMA")

            # if there's no terminator, panic!
            else:
                self.panic("keyword needs terminator")

        if not self.peek("KEYWORD"):
            self.panic("expected a keyword")

        token = self.consume("KEYWORD")

        # keywords may sometimes end with a COLON.
        has_colon = self.peek("COLON")
        if has_colon:
            self.consume("COLON")

        node = {
            "type": "keyword",
            "name": token.typ
        }

        # TODO: maybe dispatch this somehow.

        if token.typ == "POP":
            node["var"] = None

            # POP may optionally save the value to an identifier.
            if has_colon:
                value = self.parse_simple()

                # make sure we're not saving to a string or something.
                if value["type"] != "identifier":
                    self.panic(f"POP expected identifier, got {value['type']}")

                node["var"] = value
                assert_terminator()  # make sure we're only popping to a name.

        elif token.typ == "PUSH":
            if not has_colon:
                self.panic("PUSH requires an expression")

            # it is possible to push a FUNCTION keyword, so handle that.
            if self.peek("FUNCTION", "__PYTHON__"):
                node["value"] = self.parse_keyword()

            # however, we would usually just push a normal expression.
            else:
                node["value"] = self.parse_expr()

        # simple keywords that just take an expression, nothing special.
        elif token.typ in ("EXIT", "CALL"):
            if not has_colon:
                self.panic(f"{token.typ} requires an expression")

            node["expr"] = self.parse_expr()

        # this is a super basic keyword. doesn't even take a value.
        elif token.typ in ("FINISH", "ERROR"):
            if has_colon:
                self.panic("unexpected expression for keyword")

            assert_terminator()

        # the equivalent of an IF statement.
        elif token.typ == "SHOULD":
            # SHOULD is followed by a expression, then a block.
            node["expr"] = self.parse_expr(has_parens=True)
            node["body"] = self.parse_block()

        # these are all just keywords with blocks after them.
        elif token.typ in ("OTHERWISE", "PROGRAM", "FUNCTION"):
            node["body"] = self.parse_block()

        # these all parse the same: they take one string for a value.
        elif token.typ in ("__PYTHON__", "INCLUDE"):
            if not has_colon:
                self.panic("expected a value for keyword")

            value = self.parse_simple()
            if value["type"] != "string":
                self.panic(f"expected string, got {value['type']}")

            node["value"] = value
            assert_terminator()

        # delete variables and stuff
        elif token.typ == "FORGET":
            if not has_colon:
                self.panic("expected a identifier to forget")

            value = self.parse_simple()
            if value["type"] != "identifier":
                self.panic(f"expected identifier, got {value['type']}")

            node["value"] = value
            assert_terminator()

        elif token.typ == "MODULE":
            if not has_colon:
                self.panic("MODULE needs a name")

            value = self.parse_simple()
            if value["type"] != "identifier":
                self.panic("expected an identifier for MODULE")

            node["identifier"] = value
            node["body"] = self.parse_block()

        # leave all other registered keywords alone. they are used
        # as values in expressions, like `ARGS - 1`.
        elif token.typ in KEYWORDS:
            if has_colon:
                self.panic(f"unexpected expression for {token.typ}")

        # if we've been hit with anything else, bugger off :D
        else:
            self.panic(f"unknown keyword {token.typ!r}")

        return node
