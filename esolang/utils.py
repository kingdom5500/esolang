import re
from dataclasses import dataclass

# constants

KEYWORDS = (
    "ARGS",
    "CALL",
    "FORGET",
    "DEPTH",
    "ERROR",
    "EXIT",
    "FINISH",
    "FUNCTION",
    "INCLUDE",
    "MODULE",
    "OTHERWISE",
    "POP",
    "PROGRAM",
    "PUSH",
    "SHOULD",
    "__PYTHON__"
)

TOKENS = (
    # literals
    ("NUMBER", r"\d+(\.\d+)?"),    # any integer or float literal
    ("STRING", r"(?P<QUOT>[\"'])"  # an opening quote, called "QUOT"
               r".*"               # the string's content
               r"(?<!\\)"          # if there's not an escape \
               r"(?P=QUOT)"),      # match the same char as "QUOT"

    # operators
    ("OP", r"[><!=]=|"         # equality operators
           r"[><+\-/*%&|^]"),  # or single-char operators

    # special keyword tokens (same name as value)
    *zip(KEYWORDS, KEYWORDS),

    # syntactic types
    ("BLOCK_OPEN", r"\["),
    ("BLOCK_CLOSE", r"\]"),
    ("EXPR_OPEN", r"\("),
    ("EXPR_CLOSE", r"\)"),
    ("COLON", r":"),
    ("COMMA", r","),

    # identifiers
    ("NAME", r"\w+(.\w+)?"),

    # unimportant things
    ("NEWLINE", r"\n"),       # for tracking line numbers
    ("SKIP", r"\s+|#.*"),      # skip whitespace and comments
    ("UNKNOWN", r".")         # everything else is invalid
)

TOKEN_REGEX = re.compile(
    "|".join(fr"(?P<{name}>{regex})" for name, regex in TOKENS)
)

# the values of simple tokens don't change from eso code to compiled code.
SIMPLE_TOKENS = {
    "NUMBER": "number",
    "STRING": "string",
    "OP": "operator",
    "NAME": "identifier"
}

SIMPLE_KEYWORDS = ("ARGS", "DEPTH", "POP")

# utility classes and whatnot


@dataclass
class Token:
    """Generic class for tokens."""
    typ: str
    value: str
    line: int
    column: int

# exceptions


class EsoError(Exception):
    def __init__(self, message, line_no="N/A", col_no="N/A"):
        self.message = f"{message} (line {line_no}, column {col_no})"

    def __str__(self):
        return self.message
