import os.path
import textwrap

from esolang.compiler import parse_module
from esolang.utils import EsoError, SIMPLE_KEYWORDS, SIMPLE_TOKENS


class Transformer:

    def __init__(self):
        self.modules = []

    def transform(self, node):
        """
        Transform an entire program to python code.
        """

        if node["name"] != "PROGRAM":
            raise EsoError("expected a PROGRAM keyword")

        start = (
            "_eso_stack = __import__('sys').argv.copy()\n"
            "_program_args = len(_eso_stack)\n"
        )

        end = (
            f"{self.transform_keyword(node)}\n"
            "_eso_program(_program_args)"
        )

        module_codes = []

        # modules can import modules, so a for loop won't work
        index = 0
        while index < len(self.modules):
            # compile the module
            node = parse_module(self.modules[index])
            compiled = self.transform_keyword(node)
            module_codes.append(compiled)

            # move on to the next module
            index += 1

        return start + "\n".join(module_codes) + end

    def transform_simple(self, node):
        """
        Transform a simple node, such as a string.
        """

        if node["type"] in SIMPLE_TOKENS.values():
            return node["value"]

        elif node["type"] == "keyword":
            if node["name"] in SIMPLE_KEYWORDS:
                return self.transform_keyword(node)

        raise EsoError("expected a simple token")

    def transform_expr(self, node):
        """
        Transform an expression.
        """

        items = []

        for item in node["body"]:
            if item["type"] == "expression":
                items.append(self.transform_expr(item))

            else:
                items.append(self.transform_simple(item))

        content = " ".join(items)

        if node["parens"]:
            content = f"({content})"

        return content

    def transform_keyword(self, node):
        """
        Transform a keyword.
        """

        keyword = node["name"]
        content = ""

        if keyword == "PROGRAM":
            content += "def _eso_program(_ARGS):\n"
            content += self.transform_block(node["body"])

        # transforms to `name = _eso_stack.pop()`
        elif keyword == "POP":
            # if this is an assignment...
            if node["var"] is not None:
                identifier = self.transform_simple(node["var"])

                if identifier != "null":
                    content += f"{identifier} = "

            content += "_eso_stack.pop()"

        elif keyword == "PUSH":
            body = node["value"]

            if body["type"] == "expression":
                value = self.transform_expr(body)

            elif body["type"] == "keyword":
                if body["name"] == "FUNCTION":
                    content += self.transform_keyword(body)
                    value = "_eso_func"

                elif body["name"] == "__PYTHON__":
                    value = self.transform_keyword(body)

                else:
                    raise EsoError("unexpected keyword")
            else:
                raise EsoError("unexpected token")

            content += f"_eso_stack.append({value})"

        # transforms to `exit(status)`
        elif keyword == "EXIT":
            status = self.transform_expr(node["expr"])
            content += f"exit({status})"

        # transforms to `__eso_stack.pop(-args)(args)`
        elif keyword == "CALL":
            args = self.transform_expr(node["expr"])
            content += f"_eso_stack.pop(-({args} + 1))({args})"

        # a regular function definition
        elif keyword == "FUNCTION":
            content += "def _eso_func(_ARGS):\n"
            content += self.transform_block(node["body"])

        # a regular `if` statement
        elif keyword == "SHOULD":
            condition = self.transform_expr(node["expr"])
            content += f"if {condition}:\n"
            content += self.transform_block(node["body"])

        # a regular `else` statement
        elif keyword == "OTHERWISE":
            content += f"else:\n"
            content += self.transform_block(node["body"])

        # turns the ARGS keyword to the _ARGS variable
        elif keyword == "ARGS":
            content += "_ARGS"

        # finds the depth of the stack
        elif keyword == "DEPTH":
            content += "len(_eso_stack)"

        # equivalent to a return statement
        elif keyword == "FINISH":
            content += "return"

        # include modules. they must be written in this language.
        elif keyword == "INCLUDE":
            # get the file name, remove surrouding quotes.
            quoted = self.transform_simple(node["value"])
            full_name = quoted[1:-1]

            path, file = os.path.split(full_name)
            name, extension = os.path.splitext(file)

            if extension:
                raise EsoError("include without extensions please")

            if name not in self.modules:
                self.modules.append(name)

        # transforms to `eval(...)`
        elif keyword == "__PYTHON__":
            string = self.transform_simple(node["value"])
            content += f"eval({string})"

        # raises an exception
        elif keyword == "ERROR":
            content += "raise Exception(_eso_stack.pop())"

        # deletes a variable
        elif keyword == "FORGET":
            identifier = self.transform_simple(node["value"])
            content += f"del {identifier}"

        elif keyword == "MODULE":
            name = self.transform_simple(node["identifier"])
            content += f"class {name}:\n"
            content += self.transform_block(node["body"])

        # everything else is BAD
        else:
            raise EsoError("unknown keyword")

        return content

    def transform_block(self, body):
        """
        Transform a block, like a function
        """

        content = ""
        for item in body:
            code = textwrap.indent(
                self.transform_keyword(item), " "
            )

            content += f"{code}\n"

        return content
