from esolang.utils import EsoError, Token, TOKEN_REGEX


def tokenise(source):
    """Convert code to a list of Token objects."""

    line_no = 1
    line_start = 0

    # iterate over every match we find
    for match in TOKEN_REGEX.finditer(source):
        typ = match.lastgroup
        value = match.group(typ)
        col_no = match.start() - line_start

        # count new lines for debugging
        if typ == "NEWLINE":
            line_start = match.end()
            line_no += 1

        # this is a bad token, kill it
        elif typ == "UNKNOWN":
            raise EsoError(
                f"unexpected token {value!r}", line_no, col_no
            )

        # if it's not a useless token, yield it!
        elif typ != "SKIP":
            yield Token(typ, value, line_no, col_no)
